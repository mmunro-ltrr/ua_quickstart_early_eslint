(function ($, Drupal, window, document, undefined) {

  Drupal.behaviors.bootstrap_style_overrides = {
    attach: function(context, settings) {
      // overrides when to body scroll and menu scroll
      $('.overlay-menu-scroll').on('show.bs.collapse', function() {
        $('body').addClass('overflow-hidden');
      });

      $('.overlay-menu-scroll').on('hide.bs.collapse', function() {
        $('body').removeClass('overflow-hidden');
      });
    }
  };

  Drupal.behaviors.bootstrap_click_overrides = {
    attach: function(context, settings) {
      // fixes full width parent menu items to be clickable
      if( $(window).width() >= 991 ) {
        $('.overlay-menu-scroll ul li.dropdown > a').removeAttr('data-toggle');
      }
      if( $(window).width() < 991 ) {
        $('.overlay-menu-scroll ul li.dropdown > a').attr('data-toggle', 'dropdown');
      }
      $(window).resize(function() {
        if( $(window).width() >= 991 ) {
          $('.overlay-menu-scroll ul li.dropdown > a').removeAttr('data-toggle');
        }
        if( $(window).width() < 991 ) {
          $('.overlay-menu-scroll ul li.dropdown > a').attr('data-toggle', 'dropdown');
        }
      });
    }
  };

  Drupal.behaviors.parent_menu_clone = {
    attach: function(context, settings) {
      // adds clone of parent menu item
      var primaryDropdownMenuItemContents = $('.overlay-menu-scroll ul li.dropdown > a');
      $.each(primaryDropdownMenuItemContents, function(index, value) {
        var hrefValue = primaryDropdownMenuItemContents[index];
        var newLink = '<li class="uaqs-cloned-link hidden-lg hidden-md"><a href="' + hrefValue + '">Overview</a></li>';
        var dropdownUnorderedList = $(value).parent();
        var nolinkMenuItem = $(value).hasClass('nolink');

        if (nolinkMenuItem) {
          return;
        } else {
          $(dropdownUnorderedList).children('.dropdown-menu').prepend(newLink);
        }
      });
    }
  };

  Drupal.behaviors.animate_mobile_scroll = {
    attach: function (context, settings) {
      // Get the scroll width
      var $elem = $('.navbar .menu.nav');
      var maxScrollLeft = $elem.get(0).scrollWidth - $elem.get(0).clientWidth;

      // Scroll all the way right first, then delay and scroll left.
      $elem.scrollLeft(maxScrollLeft);
      $elem.delay(2000).animate({
        scrollLeft: 0
      }, 500);
    }
  };

  Drupal.behaviors.overlay_menu = {
    attach: function(context, settings) {
      // Listen to see if any menu items are clicked
      // This should run after resources menu item has been moved into the scroll nav
      $( window ).load(function() {
        $('.menu__item.is-expanded.expanded.dropdown > a').click(function() {
          openNav();
        });
      });

      // Opens overlay menu
      function openNav(){
        $('.overlay-menu-scroll-toggle button.navbar-toggle').removeClass('collapsed');
        $('.overlay-menu-scroll-toggle button.navbar-toggle').attr('aria-expanded', 'true');
        $('.overlay-menu-scroll').addClass('in');
        $('.overlay-menu-scroll').attr('aria-expanded', 'true');
      }
    }
  };

  Drupal.behaviors.dropdown_accessibility = {
    attach: function(context, settings) {
      $(window).on("load resize",function(e){
        if( $(window).width() >= 991 ) {
          $('.navbar-nav').on('focusin', '.dropdown > a', function (e) {
            $(this).parent('.menu__item').addClass('open').siblings('.menu__item').removeClass('open');
          });
        }
      });
    }
  };

})(jQuery, Drupal, this, this.document);
