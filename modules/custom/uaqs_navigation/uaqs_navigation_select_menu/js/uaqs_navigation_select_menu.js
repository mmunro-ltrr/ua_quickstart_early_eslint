(function ($, Drupal, window, document, undefined) {

  Drupal.behaviors.uaqsNavigationSelectMenuGotoSelection = {
    attach: function (context, settings) {
      //  uaqs_navigation_select_menu form id's are added in an array depending
      //  on th page you are on, and how many select menus are on the page.
      for ( var i = 0; i < settings.uaqsNavigationSelectMenu.ids.length; i++ ) (function (i) {
        var selectFormId = settings.uaqsNavigationSelectMenu.ids[i];
        var selectForm = document.getElementById(selectFormId);
        var selectButton = selectForm.getElementsByClassName('js_select_menu_button')[0];
        //  When you click on the button, get the selected value of the form
        //  that contains the button and go to that page.
        selectButton.onclick = function (e) {
          var index = selectForm.firstElementChild.getElementsByClassName('select-primary')[0].selectedIndex
          var selectHref = selectForm.firstElementChild.getElementsByClassName('select-primary')[0][index].dataset.href
          //  Don't follow link if using the nolink setting
          if (selectHref.indexOf('%3Cnolink%3E') <= 0) {
            // If the link works, don't allow the button to focus.
            e.stopImmediatePropagation();
            window.location = selectHref;
          }
          else {
            // If the selected item isn't a link, show the popup.
            $('#' + selectFormId).popover('show')
          }
        }
      })(i);
    }
  };
})(jQuery, Drupal, this, this.document);
